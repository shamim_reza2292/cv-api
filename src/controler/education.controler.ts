
import {educationInfo} from "../interfaces/educationInfo";
import { Request, Response, NextFunction } from 'express';

const educationModal = require('../modal/education');

export class educationControler {
    public static educationPost(req: Request, res: Response, next: NextFunction) {
        const passingYear = req.body.passingYear;
        const nameOfDegree = req.body.nameOfDegree;
        const nameOfInstitute = req.body.nameOfInstitute;
        const eduId = req.body.id;
    
        const educationData: educationInfo = new educationModal({
            passingYear,
            nameOfDegree,
            nameOfInstitute
        });

        if(eduId){
            educationModal.findById(eduId).then((eduData: educationInfo)=>{
                eduData.nameOfDegree = nameOfDegree;
                eduData.nameOfInstitute = nameOfInstitute;
                eduData.passingYear = passingYear;
                eduData.save().then((data: any)=>{
                    res.status(201).json({
                        message: 'update successfuly'
                    })
                }).catch((err: any)=>{
                    console.log(err);
                    
                })
            }).catch((err: any)=>{
                console.log(err);
                
            })

        }else{
            educationData.save().then((data: educationInfo) =>{
                res.status(201).json({
                    message: "education data successfully save",
                    data
                })
            }).catch((err: Error) =>{
                console.log(err);
            });
        }
    
       
    }


    public static getEducations (req: Request, res: Response, next: NextFunction) {
        // req.session.loggedIn = true;
        // console.log(req.session);
        
        educationModal.find().then((eduData: educationInfo)=>{
            res.status(201).json({
                message: "get data successfully",
                data: eduData
            })
        }).catch((err: Error)=>{
            console.log(err);
        })
    }


}

