const skilModal = require('../modal/skil');

import {Request, Response, NextFunction} from 'express';
 
import { skilInfo } from '../interfaces/skilinfo';


export class skilControler {

    public static postSkil (req: Request, res: Response, next: NextFunction) {
        const skilName = req.body.skilName;
        const confidentLavel = req.body.confidentLavel;
        const skilId = req.body.id;
        const skilData: skilInfo = new skilModal({
            skilName,
            confidentLavel
        });

        if(skilId){
            skilModal.findById(skilId).then((data: skilInfo)=>{
                data.skilName = skilName;
                data.confidentLavel = confidentLavel;
                data.save().then((skil: skilInfo)=>{
                    res.status(200).json({
                        message: "Successfully update skil.",
                        // skilData: skil
                    })
                }) 

            }).catch((err: any)=>{
                console.log(err);
                
            })


        }else{
            skilData.save().then((data: skilInfo) =>{
                res.status(201).json({
                    message: "Successfully post skil.",
                    // skilData: data
                })
            }).catch((err:any)=>{
        
            });
        }

        
    }


    public static getSkil (req: Request, res: Response, next: NextFunction) {
        skilModal.find().then((data: skilInfo)=>{
            res.status(201).json({
                data
            })
        }).catch((err:any)=>{
            console.log(err);
        })
    }





}

