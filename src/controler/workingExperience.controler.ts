import { Request, Response, NextFunction } from "express";
import { workingExperienceInfo } from "../interfaces/workingExperienceInfo";



const workingExperienceModal = require('../modal/workingExperience');
const dateFormat = require('../helper/dateFormeter');

 
// es6 module

export class workingExperienceControler {

    public static postWorkingExperience (req: Request, res: Response , next: NextFunction) {
        const companyName  = req.body.companyName;
        const designation = req.body.designation;
        const responsibilities = req.body.responsibilities;
        // const startDate = dateFormat.dateString(req.body.startDate);
        const startDate = req.body.startDate;
        // const endDate = dateFormat.dateString(req.body.endDate);
        const endDate = req.body.endDate;
        const continuing = req.body.continuing;
        const workingExperienceId = req.body.id; 
    
        const workingExperienceData: workingExperienceInfo = new workingExperienceModal({
            companyName,
            designation,
            responsibilities, 
            startDate,
            endDate, 
            continuing
        });

        if(workingExperienceId){
            workingExperienceModal.findById(workingExperienceId).then((updateExperience: workingExperienceInfo)=>{
                updateExperience.companyName = companyName;
                updateExperience.designation = designation ;
                updateExperience.responsibilities = responsibilities;
                updateExperience.startDate = startDate;
                updateExperience.endDate = endDate;
                updateExperience.continuing = continuing;
                updateExperience.save().then(data=>{
                    res.status(201).json({
                        message: 'update successfully.'
                    })
                })
            })
        }else{
            workingExperienceData.save().then((result: workingExperienceInfo)=>{
                res.status(201).json({
                    message: "successfully psot working experience.",
                    // data: result
                })
            })
        }
    
            
    }


    public static getWorkingExperience (req: Request, res: Response, next: NextFunction) {
        workingExperienceModal.find().then((data:any)=>{
            res.status(201).json({
                data
            })
        }).catch((err:any)=>{
            console.log(err);
        })
    }





}


