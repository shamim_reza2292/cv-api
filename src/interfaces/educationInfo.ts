import { Document } from 'mongoose';

export declare interface educationInfo extends Document {
    passingYear:   Number,
    nameOfDegree: String,
    nameOfInstitute: String
}

