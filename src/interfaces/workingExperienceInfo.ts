

import { Document } from "mongoose";

export interface workingExperienceInfo extends Document{
    companyName:  String,
    designation: String,
    responsibilities: String,
    startDate: String,
    endDate: Date,
    continuing: Boolean
}

