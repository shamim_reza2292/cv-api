// const mongooseDb = require('mongoose');

import {Schema} from 'mongoose';
import Mongoose  from 'mongoose';

import {educationInfo} from '../interfaces/educationInfo';

// const schema = mongooseDb.Schema;


const educationSchema = new Schema({
    passingYear: {type: Number, required: true},
    nameOfDegree: {type:String, required: true},
    nameOfInstitute: {type: String, required: true}
})


module.exports = Mongoose.model<educationInfo>('education', educationSchema);
