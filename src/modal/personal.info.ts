 
import { Document, Schema, Model, model} from "mongoose";
import mongoose from 'mongoose'
import {perosnalInfo} from '../interfaces/personalinfo'

const personalInfoSchema = new Schema({
    name: {type: String},
    phoneNumbeer: {type: Number, required: true},
    description: {type: String, required: true},
    email: {type: String, required: true},
    address: {type: String, required: true},
    socialLink: {type: String, required: true},
    designation: {type: String, required: true},
    user: {type: Schema.Types.ObjectId, ref: 'users' }
}); 

// export class personalInfo extends Model{
    
// }

module.exports = mongoose.model<perosnalInfo>("personalInfo", personalInfoSchema);
// module.exports = model<perosnalInfo>("personalInfo", personalInfoSchema);
