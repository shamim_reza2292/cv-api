// const mongoosedb = require('mongoose');
// const skilSchema = mongoosedb.Schema;


import mongoose from "mongoose";
import {Schema} from 'mongoose' ;
import { skilInfo } from "../interfaces/skilinfo";



const skilSchemaInfo = new Schema({
    skilName: {type: String, required: true},
    confidentLavel: {type: Number, required: true}
});


module.exports = mongoose.model<skilInfo>('skil', skilSchemaInfo);

