// const mongooses = require('mongoose');
// const experienceSchema = mongooses.Schema;


import {Schema} from 'mongoose';
import mongoose from 'mongoose';
import {workingExperienceInfo} from "../interfaces/workingExperienceInfo";

const workingExperienceSchema = new Schema({
    companyName: {type: String, required: true},
    designation: {type: String, required: true},
    responsibilities: {type: String, required: true},
    startDate: {type: Date, required: true},
    endDate: {type: Date},
    continuing: {type: Boolean}
});


module.exports = mongoose.model<workingExperienceInfo>('workingExperience', workingExperienceSchema);

